 
//Write interface driver
 class FIFODriver extends uvm_driver#(TransPacket);
 
   virtual FIFOIntf_in vif;
   
   `uvm_component_utils(FIFODriver)
   
   function new(string name = "FIFODriver", uvm_component parent = null);
     super.new(name, parent);
   endfunction
   
   virtual function void build_phase(uvm_phase phase);
     super.build_phase(phase);
     if(! uvm_config_db#(virtual FIFOIntf_in)::get(this, "", "vif", vif))
       `uvm_fatal("NOVIF",{"virtual intf not set for", get_full_name(), ".vif"})
	   
   endfunction
   
   virtual task run_phase(uvm_phase phase);
     reset_dut();
     forever begin
	   seq_item_port.get_next_item(req);
	   drive_transaction(req);
	   seq_item_port.item_done();
	 end
   endtask
   
     task reset_dut();
       vif.rst = 0;
       #5ns ;
       vif.rst = 1;
       vif.wr_en = 0;
       vif.data_in = 0;
     endtask
   
   task drive_transaction( TransPacket req);
     if(vif.rst) begin
       @(negedge vif.clk);
	   vif.wr_en = 1;
       $display($time, "driving data_in = %0x", req.data);
	   vif.data_in = req.data;
	   #8ns ;
	  vif.wr_en = 0;
     end
   endtask
 endclass
     
//Write Interface Error Driver
class FIFOErrorDriver extends FIFODriver;
  
  `uvm_component_utils(FIFOErrorDriver)
  
  function new(string name = "FIFOErrorDriver", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  virtual task run_phase(uvm_phase phase);
    //reset dut
    vif.rst = 0;
    #5ns ;
    //reset release
    vif.rst = 1;
    forever begin 
      seq_item_port.get_next_item(req);
      drive_transaction(req);
      seq_item_port.item_done();
    end
    
  endtask
  
  virtual task drive_transaction(TransPacket req);
    
    @(posedge vif.clk) ;
    vif.wr_en = 1;
    $display("DRIVING ERROR DATA");
    vif.data_in = - req.data;
    
    @(posedge vif.clk);
    vif.wr_en = 0;
  endtask
  
  
endclass

//Read Interface Driver 
class FIFODriver_O extends uvm_driver#(TransPacket);
 
   virtual FIFOIntf_out vif;
   
   `uvm_component_utils(FIFODriver_O)
   
   function new(string name = "FIFODriver_O", uvm_component parent = null);
     super.new(name, parent);
   endfunction
   
   virtual function void build_phase(uvm_phase phase);
     super.build_phase(phase);
     if(! uvm_config_db#(virtual FIFOIntf_out)::get(this, "", "vif", vif))
       `uvm_fatal("NOVIF",{"virtual intf not set for", get_full_name(), ".vif"})
	   
   endfunction
   
   virtual task run_phase(uvm_phase phase);
     reset_dut();
     forever begin 
	   seq_item_port.get_next_item(req);
	   drive_transaction(req);
	   seq_item_port.item_done();
	 end
   endtask
     task reset_dut();
       vif.rd_en = 0;
     endtask
   task drive_transaction( TransPacket req);
       #20ns ;
     @(negedge vif.clk);
	   vif.rd_en = 1;
       $display($time, "driving rd signal");
     #8ns ;  
	   vif.rd_en = 0;
   endtask
 endclass

     