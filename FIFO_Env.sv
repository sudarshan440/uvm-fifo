 
class FIFOEnv extends uvm_env;
 
    FIFOAgentIn agent_in;
	FIFOAgentOut agent_out;
	FIFOScoreboard scoreboard;
	
	`uvm_component_utils(FIFOEnv)
	
	function new(string name = "FIFOEnv", uvm_component parent = null);
	  super.new(name, parent);
	endfunction
	
	virtual function void build_phase(uvm_phase phase);
	  agent_in = FIFOAgentIn::type_id::create("agent_in", this);
	  agent_out = FIFOAgentOut::type_id::create("agent_out", this);
	  scoreboard = FIFOScoreboard::type_id::create("scoreboard", this);
	endfunction
	
	virtual function void connect_phase(uvm_phase phase);
      agent_out.monitor.items_collected_port.connect(scoreboard.act_data_collected_port.analysis_export);
      
 agent_in.monitor.items_collected_port.connect(scoreboard.exp_data_collected_port.analysis_export);

	endfunction
	
endclass
	  
	