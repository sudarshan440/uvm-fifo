
//TRANS PACKET
class TransPacket extends uvm_sequence_item;
  rand byte data;
  rand bit rd_en;
  rand bit wr_en;
  
  `uvm_object_utils(TransPacket)
  
  function new(string name="TransPacket");
    super.new(name);
  endfunction
  
  constraint data_c { data >= 0 ;}

endclass 


 
//Write interface(Intf_in) sequencer
class FIFOSequencer extends uvm_sequencer#(TransPacket);
  `uvm_component_utils(FIFOSequencer)
  
  function new(string name = "FIFOSequencer", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
endclass

//Intf_in sequence
class FIFOSequence extends uvm_sequence;
   TransPacket item;
   `uvm_object_utils(FIFOSequence)
   
  function new(string name = "FIFOSequence");
      super.new(name);
   endfunction
	 
	virtual task body();
	  item = TransPacket::type_id::create("item");
	  
      repeat(5) begin
	    start_item(item);
        void'(item.randomize());
		finish_item(item);
	  end
	endtask
 endclass

//Read Interface (Intf_out) sequencer
class FIFOSequencer_out extends uvm_sequencer#(TransPacket);
  `uvm_component_utils(FIFOSequencer_out)

  function new(string name = "FIFOSequencer_out", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
endclass

//Read interface(Intf_out) sequence
class FIFOSequence_out extends uvm_sequence;
   TransPacket item;
   
  `uvm_object_utils(FIFOSequence_out)

  function new(string name = "FIFOSequence_out");
      super.new(name);
   endfunction
	 
	virtual task body();
	  item = TransPacket::type_id::create("item");
	  
      repeat(5) begin
	    start_item(item);
        void'(item.randomize(rd_en));
		finish_item(item);
	  end
	endtask
endclass


//virtual sequence
class FIFOVirSequence extends uvm_sequence;
   FIFOSequencer seqr_in;
   FIFOSequencer_out seqr_out;
  
   FIFOSequence seq_in;
   FIFOSequence_out seq_out;
  
  `uvm_object_utils(FIFOVirSequence)
   
  function new(string name = "FIFOVirSequence");
      super.new(name);
  endfunction
	 
  virtual task body();
      seq_in = FIFOSequence::type_id::create("seq_in");
      seq_out = FIFOSequence_out::type_id::create("seq_out");
      fork
        seq_in.start(.sequencer(seqr_in), .parent_sequence(this));
        seq_out.start(.sequencer(seqr_out), .parent_sequence(this));
      join
  endtask 
endclass


