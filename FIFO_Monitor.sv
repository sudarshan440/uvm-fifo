 
//Write Interface Monitor
 class FIFOMonitor extends uvm_monitor;
 
   virtual FIFOIntf_in vif;
   TransPacket item;
   uvm_analysis_port#(TransPacket) items_collected_port;
   `uvm_component_utils(FIFOMonitor)
   
   function new(string name = "FIFOMonitor", uvm_component parent = null);
     super.new(name, parent);
     items_collected_port = new("items_collected_port", this);
   endfunction
   
   virtual function void build_phase(uvm_phase phase);
     super.build_phase(phase);
     if(! uvm_config_db#(virtual FIFOIntf_in)::get(this, "", "vif", vif))
       `uvm_fatal("NOVIF",{"virtual intf not set for", get_full_name(), ".vif"})
	   
   endfunction
   
   virtual task run_phase(uvm_phase phase);
     forever begin
	   item = TransPacket::type_id::create("item");
       @(posedge vif.clk);
       if(vif.wr_en==1) begin	
	   item.data = vif.data_in;
       $display($time, " MON: writing exp data = %0x to SCB", item.data);
	   items_collected_port.write(item);
       end
	 end
   endtask
   
 endclass
     
 //Read Interface Monitor
 class FIFOMonitor_O extends uvm_monitor;
 
   virtual FIFOIntf_out vif;
   TransPacket item;
   uvm_analysis_port#(TransPacket) items_collected_port;
   `uvm_component_utils(FIFOMonitor_O)
   
   function new(string name = "FIFOMonitor_O", uvm_component parent = null);
     super.new(name, parent);
     items_collected_port = new("items_collected_port", this);
   endfunction
   
   virtual function void build_phase(uvm_phase phase);
     super.build_phase(phase);
     
     if(! uvm_config_db#(virtual FIFOIntf_out)::get(this, "", "vif", vif))
       `uvm_fatal("NOVIF",{"virtual intf not set for", get_full_name(), ".vif"})
	      
   endfunction
   
   virtual task run_phase(uvm_phase phase);
     
     item = TransPacket::type_id::create("item");

     forever begin
       @(posedge vif.clk);
       if(vif.rd_en==1) begin
         #1 item.data = vif.data_out;
         $display($time, " MON: writing act data = %0x to SCB", item.data);
  
	     items_collected_port.write(item);
       end
	 end
   endtask 
   
 endclass