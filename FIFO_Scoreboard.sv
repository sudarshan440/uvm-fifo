
class FIFOScoreboard extends uvm_scoreboard;
 
    TransPacket exp_data;
	TransPacket act_data;
	
	uvm_tlm_analysis_fifo #(TransPacket) exp_data_collected_port;
	uvm_tlm_analysis_fifo #(TransPacket) act_data_collected_port;
	 
	`uvm_component_utils(FIFOScoreboard)
	
  
    function new(string name = "FIFOScoreboard", uvm_component parent = null);
	   super.new(name, parent);
	   
	   exp_data_collected_port = new("exp_data_collected_port", this);
	   act_data_collected_port = new("act_data_collected_port", this);
	endfunction
	
    virtual task run_phase(uvm_phase phase);
	   forever begin
         //phase.raise_objection(this);

	       exp_data = TransPacket::type_id::create("exp_data");
           act_data = TransPacket::type_id::create("act_data");
         

	       exp_data_collected_port.get(exp_data);
	       act_data_collected_port.get(act_data);

         $display("exp data = %0x , act data = %0x ", exp_data.data, act_data.data);
         
	       if(exp_data.data == act_data.data)
             `uvm_info("SB", "data mactched ", UVM_LOW)
	       else
             `uvm_error("SB","data mismatched")

        //  phase.drop_objection(this);
       end
         
	 endtask

endclass
	
	
	