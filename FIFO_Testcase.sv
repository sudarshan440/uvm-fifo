 class FIFOTest extends uvm_test;
   FIFOEnv env;
   FIFOVirSequence v_seq;
   `uvm_component_utils(FIFOTest)
   
   function new(string name = "FIFOTest", uvm_component parent = null);
     super.new(name, parent); 
   endfunction
   
   virtual function void build_phase(uvm_phase phase);
       super.build_phase(phase);
     uvm_config_db#(uvm_active_passive_enum)::set(this, "env.agent_in*", "is_active", UVM_ACTIVE);
     uvm_config_db#(uvm_active_passive_enum)::set(this, "env.agent_out*", "is_active", UVM_ACTIVE);
     
     //FIFODriver::type_id::set_type_override(FIFOErrorDriver::get_type());
     //FIFODriver::type_id::set_inst_override(FIFOErrorDriver::get_type(), "*agent_in.*");
	   env = FIFOEnv::type_id::create("env", this);

   endfunction
   
   
   virtual function void end_of_elaboration_phase(uvm_phase phase);
     //printing the topology
     print();
   endfunction
   
	virtual task run_phase(uvm_phase phase);
      v_seq = FIFOVirSequence::type_id::create("FIFOVirSequence");

	   phase.raise_objection(this);
       v_seq.seqr_in = env.agent_in.sequencer;
       v_seq.seqr_out = env.agent_out.sequencer;
       
      v_seq.start(.sequencer(null));
      #100ns ;
	   phase.drop_objection(this);
      
	endtask
endclass
	  
	   
   