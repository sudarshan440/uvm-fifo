`include "uvm_macros.svh"

import uvm_pkg::*;
`include "FIFO_sequence.sv"
`include "FIFO_Driver.sv"
`include "FIFO_Monitor.sv"
`include "FIFO_Agent_in.sv"
`include "FIFO_Scoreboard.sv"
`include "FIFO_Env.sv"
`include "FIFO_Testcase.sv"


//Write Interface
interface FIFOIntf_in;
  logic clk,rst,wr_en;
  logic [7:0] data_in;
endinterface

//Read Interface
interface FIFOIntf_out;
  logic clk,rd_en,full,empty;
  logic [7:0] data_out;
endinterface


module top;
  reg clk;
 
  FIFOIntf_in intf_in();
  FIFOIntf_out intf_out();
     
  fifo dut(.clk(clk), .wr(intf_in.wr_en), .in(intf_in.data_in), .reset(intf_in.rst), .rd(intf_out.rd_en), .out(intf_out.data_out), .full(intf_out.full), .empty(intf_out.empty));
  

   initial begin    
      clk = 0;
	  forever begin
        #5ns clk = ~clk;
      end
	end
	
	assign intf_out.clk = clk;
	assign intf_in.clk = clk;
	
	initial begin
      uvm_config_db#(virtual FIFOIntf_in)::set(uvm_root::get(), "*agent_in*", "vif", intf_in);
      uvm_config_db#(virtual FIFOIntf_out)::set(uvm_root::get(), "*agent_out*", "vif", intf_out);
      $dumpfile("dump.vcd");
      $dumpvars(0, top);
      run_test("FIFOTest");
	end 
endmodule
  