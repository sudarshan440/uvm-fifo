

//Write interface Agent
class FIFOAgentIn extends uvm_agent;
   uvm_active_passive_enum is_active;

   FIFODriver driver;
   FIFOMonitor monitor;
   FIFOSequencer sequencer;
   `uvm_component_utils(FIFOAgentIn)
   
   function new(string name = "FIFOAgentIn" , uvm_component parent = null);
     super.new(name, parent);
   endfunction
   
   virtual function void build_phase(uvm_phase phase);
     super.build_phase(phase);
     uvm_config_db#(uvm_active_passive_enum)::get(this, " ", "is_active", is_active);
     if(is_active == UVM_ACTIVE) begin
	 driver = FIFODriver::type_id::create("driver", this);
	 sequencer = FIFOSequencer::type_id::create("sequencer", this);
     end
	 
     monitor = FIFOMonitor::type_id::create("monitor", this);

   endfunction
   
   virtual function void connect_phase(uvm_phase phase);
     if(is_active == UVM_ACTIVE) begin
     driver.seq_item_port.connect(sequencer.seq_item_export);
     end
   endfunction
   
 endclass


//Read interface agent agent
  
 class FIFOAgentOut extends uvm_agent;
   
   uvm_active_passive_enum is_active;
   
   FIFODriver_O driver;
   FIFOMonitor_O monitor;
   FIFOSequencer_out sequencer;
   
   `uvm_component_utils(FIFOAgentOut)
   
   function new(string name = "FIFOAgentOut" , uvm_component parent = null);
     super.new(name, parent);
   endfunction
   
   virtual function void build_phase(uvm_phase phase);
     super.build_phase(phase);
     uvm_config_db#(uvm_active_passive_enum)::get(this, " ", "is_active", is_active);

     if(is_active == UVM_ACTIVE) begin
	   driver = FIFODriver_O::type_id::create("driver", this);
       sequencer = FIFOSequencer_out::type_id::create("sequencer", this);
     end
	 
     monitor = FIFOMonitor_O::type_id::create("monitor", this);

   endfunction
     
   virtual function void connect_phase(uvm_phase phase);
     if(is_active == UVM_ACTIVE) begin
       driver.seq_item_port.connect(sequencer.seq_item_export);
     end
   endfunction
   
 endclass
   
   