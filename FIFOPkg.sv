
package FIFOPkg;
	import uvm_pkg::*;

 	//`include "uvm_macros.svh"
    `include "FIFO_sequence.sv"
	`include "FIFO_Driver.sv"
	`include "FIFO_Monitor.sv"
	`include "FIFO_Agent_in.sv"
	`include "FIFO_Scoreboard.sv"
	`include "FIFO_Env.sv"
	`include "FIFO_Testcase.sv"
endpackage

